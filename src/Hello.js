import React from "react";
class App extends React.Component {
	render() {
		return (
			<div
				style={{
					backgroundColor: "darkGrey",
					width: "100%",
					height: "100%",
					position: "absolute",
					alignContent: "center",
					justifyContent: "center",
					top: 0,
					left: 0
				}}
			>
				<h1>Shot Scope V3</h1>
				<img src={require("./assets/237728621.jpeg")} alt="Shot Scope V3" />
			</div>
		);
	}
}
export default App;
